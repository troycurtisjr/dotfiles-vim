
let g:airline#extensions#tabline#enabled = 1

" Make sure this gets loaded.
runtime plugin/fugitive.vim 
let g:airline#extensions#branch#enabled = 1

let g:airline#extensions#whitespace#enabled = 0
