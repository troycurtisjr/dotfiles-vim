
set nocompatible

set backspace=indent,eol,start
set incsearch
set hlsearch

filetype plugin indent on

syntax on
