

" Don't let lh-cpp make the cmdheight ginormous.
set cmdheight=1

" Use a newline before the opening brace.
:AddStyle { -b { -prio=10 -ft=c

