"=============================================================================
" File:         ftplugin/yaml.vim                                 {{{1
" Author:       Troy Curtis Jr <troy.curtis@bitsighttech.com>
let s:k_version = 001
" Created:      04th Dec 2019
"------------------------------------------------------------------------
" Description:
"       «description»
"
"------------------------------------------------------------------------
" }}}1
"=============================================================================

" Buffer-local Definitions {{{1
set wrap
set spell
" Avoid local reinclusion {{{2
if &cp || (exists("b:loaded_ftplug_yaml")
      \ && (b:loaded_ftplug_yaml >= s:k_version)
      \ && !exists('g:force_reload_ftplug_yaml'))
  finish
endif
let b:loaded_ftplug_yaml = s:k_version
let s:cpo_save=&cpo
set cpo&vim
" Avoid local reinclusion }}}2

"------------------------------------------------------------------------
" Local mappings {{{2

"inoremap <buffer> «keybinding» «action»

"------------------------------------------------------------------------
" Local commands {{{2

"command! -b -nargs=«» «CommandName» «Action»

"=============================================================================
" Global Definitions {{{1
" Avoid global reinclusion {{{2
if &cp || (exists("g:loaded_ftplug_yaml")
      \ && (g:loaded_ftplug_yaml >= s:k_version)
      \ && !exists('g:force_reload_ftplug_yaml'))
  let &cpo=s:cpo_save
  finish
endif
let g:loaded_ftplug_yaml = s:k_version
" Avoid global reinclusion }}}2
"------------------------------------------------------------------------
" Functions {{{2
" Note: most filetype-global functions are best placed into
" autoload/«your-initials»/yaml/«yaml».vim
" Keep here only the functions are are required when the ftplugin is
" loaded, like functions that help building a vim-menu for this
" ftplugin.
" Functions }}}2
"------------------------------------------------------------------------
let &cpo=s:cpo_save
"=============================================================================
" vim600: set fdm=marker:
