# My VIM configuration
This project contains all of my customization which try to get the most out of the VIM text editor.  I've used it for a long time, and the keybindings are like second nature to me.  That said, basic VIM needs a bit more to make it the ultimate editor, especially for developing software.  That said these days I actually use spacemacs for development tasks, in evil (i.e. vi bindings) mode of course!  Still nothing beats vim when you are trolling around the filesystem editing this or that text file.

# Getting Started
This project expects to be rooted at the top-level of your home directory.  Generally it is a bad idea to have a git repo at the top-level of your home directory, so I use the [vcsh](https://github.com/RichiH/vcsh) project.  This allows git control of any files in your home directory without any of the symlink hacks that other options have.  If you don't want to adopt vcsh, then you should be able to grab a zip archive and extract it into your home directory.

Check out the `vcsh` instructions in my `dotfiles-usr` project.

## Basic structure
All the plugins are managed using the `pathogen` plugin manager.  Plugins are further put into version directories under `.vim/versions`.  This allows for support of multiple disparate vim versions with the same configuration set.  The version detection is done in `~/.vimrc`.  Most things should go under `~/.vim/versions/all` unless there is some known compatibility problem, and local manual customization goes into `~/.vim/versions/*/00_local`.  

## More info
To get some more info, once things are loaded, fire up vim and execute `:Helptags`.  Once that completes you can run `:help troy-quickref` to see a vim help doc I've put together with some of my favorite features of various installed plugins.
